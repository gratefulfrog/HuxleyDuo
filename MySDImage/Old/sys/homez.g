G91               ; set movement to relative mode
G1 Z5 F200        ; move Z up 5mm
G90               ; set movement to absolute mode
;G1 X40 Y5 F2000   ; move X and Y to probe point. ADJUST X and Y values to get Z probe over bed.
;G1 X44.0 Y4.0 F2000   ;  MY VALUES on 2015 10 28 16:51
;G1 X38 Y2 F2000   ;  MY VALUES after firmware upgrade to 1.1 on 2015 11 10 15:32 removed 2016 10 29
G1 X38 Y0 F2000   ;  MY VALUES as of 2016 10 29 
G30               ; home Z, using values from G31 in config.g
G1 Z0 F200        ; move Z to Z=0
