G91               ; set movement to relative mode
G1 Z4 F200 S2        ; move z up 4mm
M558 P1           ; set probe to unmodulated mode
G1 X-150 F2000 S1 ; 2017 10 24, updated to -150 to home X
G1 X4 F600 S2     ; move slowly 4mm in the +X direction
;G1 X-10 S1       ; move slowly 10mm in the -X direction, stopping at the homing switch
G1 X-150 S1        ; move slowly 10mm in the -X direction, stopping at the homing switch
M558 P2           ; set probe to modulated mode
G1 Z-4 F200 S2      ; move Z down 4mm
G90               ; set movement to absolute mode
