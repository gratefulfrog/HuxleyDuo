G91               ; set movement to relative mode
G1 Z4 F200        ; move Z up 4mm
G1 Y-150 F2000 S1 ; home Y
G1 Y4 F600        ; move slowly 4mm +Y axis 
G1 Y-10 F200 S1   ; home Y again, slower
G1 Z-4 F200       ; move Z down 5mm
G90               ; set movement to absolute mode
