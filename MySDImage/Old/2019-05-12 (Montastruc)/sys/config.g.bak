; Configuration file for RepRap Huxley Duo
; RepRapPro Ltd
;
; Copy this file to config.g if you have an Huxley Duo
; If you are updating a config.g that you already have you
; may wish to go through it and this file checking what you
; want to keep from your old file.
; 
; For G-code definitions, see http://reprap.org/wiki/G-code
; update: 2016 04 05 20:44: removed ais compensation after replacing
; X-axis idler part; compensation will be added back after a test print
; update 2016 04 06 0:06, added the axis compensation after test print
; update 2017 06 18 18:17 per new firmware dc42 1.18
; update 2017 10 24 as per ned dc42 firmware 1.19.2

; Prologue and communications section
M111 S0                             ; Debug off
M550 Phuxley RepRapPro Huxley Duo   ; Machine name (can be anything you like).
                                    ; With DHCP enabled connect to (example) http://reprapprohuxleyduo (machine name with no spaces).
M551 Preprap                        ; Machine password (currently not used)
M540 P0xBE:0xEF:0xDE:0xAD:0xFE:0x14 ; MAC Address
;M552 P0.0.0.0                       ; Un-comment for DHCP
M552 P192.168.1.99                  ; IP address, comment for DHCP
M553 P255.255.255.0                 ; Netmask, comment for DHCP
M554 P192.168.1.1                   ; Gateway, comment for DHCP
M555 P2                             ; Set output to look like Marlin

; Movement section
M569 P0 S1                          ; Reverse the X motor
M569 P3 S0                          ; Reverse the extruder motor (T0)
M569 P4 S0                          ; Reverse the extruder motor (T1)
M350 X16 Y16 Z16 E16 I1             ; set 16x microstepping with interpolation
M574 X0 Y1 Z0                       ; MY VALUES added 2017 10 24 for dc42 firmware
                                    ; This tells the firmware that you have an endstop switch at the low end of the Y axis,
                                    ; and no endstop switches for X and Z, note the ir probe does not count as an endstop switch 
M906 X700 Y700 Z600 E800            ; Set motor currents (mA)  (not changed 1.18)
M201 X750 Y750 Z15 E750             ; Accelerations (mm/s^2)  
M203 X15000 Y15000 Z300 E3600       ; Maximum speeds (mm/min)
M566 X200 Y200 Z30 E20              ; Maximum jerk speeds mm/minute    
M208 X-15 Y-3 Z0 S1                 ; MY VALUES 2017 10 24 : set axis minima and low homing switch positions (adjust to make X=0 and Y=0 the edges of the bed)
M208 X136 Y140   S0                 ; My VALUES 201 10 24 : set the axis maxima, not Z though        
; M92 X80 Y80 Z4000                   ; set axis steps/mm from dc42 not used but maybe needed? 
M92 E641                            ; Set MY VALUES OF extruder steps per mm
G21                                 ; Work in millimetres
G90                                 ; Send absolute corrdinates...
M83                                 ; ...but relative extruder moves

; Z probe section
M558 P2                             ; Use a modulated Z probe
G31 Z1.5 P602                       ; MY VALUES obtained 2015 10 29 17:48, first time z-probe calibration!        

; Heater and thermistor section
M305 P0 R4700 T100000               ; Set the heated bed thermistor resistance at 25C, series resistor to 4K7
M305 P1 R4700                       ; Set the hot end thermistor series resistor to 4K7

; Tool definition section
M563 P0 D0 H1                       ; Define tool 0
G10 P0 S-273 R-273                  ; Set tool 0 operating and standby temperatures

; Epilogue
M556 S49.5 X-0.56 Y0.06 Z-0.23     ; MY VALUES axis compensation 2016 04 06 0:07
T0                                 ; Select the first head

;M563 P1 D1 H2                      ; Define tool 1, uncomment if you have a dual colour upgrade
;G10 P1 X19 S-273 R-273             ; Set tool 1 operating and standby temperatures, uncomment if you have a dual colour upgrade

