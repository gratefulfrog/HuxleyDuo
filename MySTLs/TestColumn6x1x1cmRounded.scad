$fn=100;

hCol = 60;
wCol = 10;
dCol = wCol;

hBase = 3;
wBase = 20;
dBase = wBase;

rCyl = 1.5;

module column(){
    roundedColumn(wCol,dCol,hCol,rCyl);
}

module base(){
    roundedColumn(wBase,dBase,hBase,rCyl);
}

module roundedColumn(xc,yc,zc,rc){
    hull(){
        cylinder(r=rc,h=zc,center=false);
        translate([xc,0,0]){
            cylinder(r=rc,h=zc,center=false);
            translate([0,yc,0]){
                cylinder(r=rc,h=zc,center=false);
                translate([-xc,0,0]){
                    cylinder(r=rc,h=zc,center=false);
                }
            }    
        }
    }
}

translate([wCol/2.,dCol/2.,0])
    column();
base();