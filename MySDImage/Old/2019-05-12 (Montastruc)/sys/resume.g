; resume.g
; updated after 1.18 firmware upgrade

; Put G/M Codes in here to run when a paused print is resumed using M24
G90
;G1 X105 Y100 F6000
G1 R1 Z2 F5000	; 2017 06 18 move to 2mm above resume point
G1 R1
M83
G1 E10 F3600
