G91               ; set movement to relative mode
G1 Z5 F200        ; move Z up 5mm
G90               ; set movement to absolute mode
M558 P2           ; set probe to modulated mode 
G1 X27 Y-3 F2000   ;  MY VALUES as of 2017 10 24
G30               ; home Z, using values from G31 in config.g

