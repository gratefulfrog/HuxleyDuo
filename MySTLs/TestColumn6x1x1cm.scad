$fn=100;

hCol = 60;
wCol = 10;
dCol = wCol;

hBase = 3;
wBase = 20;
dBase = wBase;


module base(){
    cube([wBase,dBase,hBase],center=false);
}
module column(){
    cube([wCol,dCol,hCol],center=false);
}
translate([wCol/2.,dCol/2.,0])
column();
base();