$fn=100;

cableR = 3.25;
wallThickness = 4.5;
wallHeight = 3;
baseHeight= 4;
totalHeight = baseHeight+wallHeight;

module base(){
linear_extrude(baseHeight)
  import ("bowdenGuide.dxf", layer = "outline");
}
module walls(){
  eps= 1;
translate([0,0,-wallHeight])
  linear_extrude(wallHeight+eps)
    import ("bowdenGuide.dxf", layer = "BowdenGuide");
}
//walls();

module holes(){
  translate([0,0,-baseHeight*2])
    linear_extrude(baseHeight*5)
      import ("bowdenGuide.dxf", layer = "Holes");

}
module recess(){
  recessDepth = 2;
  eps=1;
  translate([0,0,baseHeight-eps])
    linear_extrude(recessDepth+eps)
      import ("bowdenGuide.dxf", layer = "ScrewRecess");
}  

module cableHole(){
  translate([0,43,-cableR])
    rotate([90,0,0])
    cylinder(wallThickness*2,r=cableR,center=true);
}


//cableHole();

module bowdenGuide(){
  difference(){
    union(){
      base();
      walls();
    }
    union(){
      #holes();
      #recess();
      #cableHole();
    }
  }
}
bowdenGuide();