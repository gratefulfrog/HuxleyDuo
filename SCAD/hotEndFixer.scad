$fn=100;

hotEndFrontSTLFileName = "../Parts/STLs/614 front hot end mount.STL";

pressDXFFileName       = "../Parts/DXF/614 bearing press.dxf";
pressLayer             = "press";
ringLayer              = "Ring";
ringSupportLayer       = "RingSupport";

pressZ = 3.2;
ringZ  = 1.5;
ringOffsetZ = 2;
pressOffset = 0;
pressX      = 2;
pressXDiff  = 40;

module inputer(){
  projection(cut=true)
    rotate([0,-90,0])
      translate([0,-11.31,-3])
        import(hotEndFrontSTLFileName);
}

module press(){
  linear_extrude(pressZ)
    import(pressDXFFileName,layer=pressLayer);
}

press();

module ring(){
  rotate([0,90,0])
    translate([-ringOffsetZ,0,0])
      linear_extrude(ringZ)
        import(pressDXFFileName,layer=ringLayer);
}
//ring();

module ringSupportL(){
  translate([-(pressX+pressOffset),0,-pressOffset])
  rotate([0,90,0])
  union(){
    linear_extrude(pressX)
      import(pressDXFFileName,layer=ringSupportLayer);
    linear_extrude(ringZ+pressX)
      import(pressDXFFileName,layer=ringLayer);
  }
}

ringSupportL();

module ringSupportR(){
  translate([0,0,-pressOffset])
  translate([pressXDiff-pressOffset+pressX,0,0])
  mirror([1,0,0])
  rotate([0,90,0])
  union(){
    linear_extrude(pressX)
      import(pressDXFFileName,layer=ringSupportLayer);
    linear_extrude(ringZ+pressX)
      import(pressDXFFileName,layer=ringLayer);
  }
}
ringSupportR();
