M561
G1 Z10 F200              ; move 10mm off the bed
;G1 X44.0 Y4.0 F2000      ; MY VALUES first probe point
;G1 X38 Y2 F2000        ; MY VALUES after firmware upgrade to 1.1 first probe point
G1 X38 Y0 F2000        ; MY VALUES as of 2016 10 29
G30 P0 Z-99999           ; probe the bed
G1 Z10 F200              ; move back up
;G1 X44.0 Y130.0 F2000    ; MY VALUES 2nd probe point
G1 X38 Y130 F2000        ; MY VALUES after firmware upgrade to 1.1 2nd probe point
G30 P1 Z-99999           ; probe the bed
G1 Z10 F200              ; move back up
;G1 X150.0 Y130.0 F2000   ; MY VALUES 3rd probe point
;G1 X144 Y130 F2000       ; MY VALUES after firmware upgrade to 1.1 3rd probe point
G1 X148 Y130 F2000       ; MY VALUES as of 2016 10 29
G30 P2 Z-99999           ; probe the bed
G1 Z10 F200              ; move back up
;G1 X150.0 Y4.0 F2000     ; MY VALUES 4th probe point
;G1 X144 Y2 F2000         ; MY VALUES after firmware upgrade to 1.1 4th probe point
G1 X148 Y0 F2000         ; MY VALUES as of 2016 10 29
G30 P3 S Z-99999         ; probe the bed, set the bed equation
G4 P1500                 ; pause to let the web interface catch up
G1 Z10 F200              ; move back up

