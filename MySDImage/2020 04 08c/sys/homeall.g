G91               ; set movement to relative mode
G1 Z5 F200        ; move Z up 5mm
; X and Y homing
M558 P1           ; set probe to unmodulated mode
G1 X-150 Y-150 F2000 S1 ; 2017 10 24, updated to home X and Y
G1 X4 Y4 F600      ; move slowly 6mm in +X and +Y directions
G1 X-10 Y-10 S1    ; move up to 10mm in the -X and -Y directions until the homing switches are triggered
; Z homing
G90                ; set movement to absolute mode
;M558 P2            ; set probe to modulated mode
M558 P2 F20         ; set probe to modulated mode feed rate 20mm/min 2020 03 30 19:00 
;G1 X27 Y-3 F2000   ;  MY VALUES as of 2017 10 24
G1 X22 Y-3 F2000   ;  MY VALUES as of 2020 03 17:10
G30                ; home Z, using values from G31 in config.g

