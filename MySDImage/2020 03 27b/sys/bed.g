M561                         ; clear any existing bed transform
G91                          ; set movement to relative mode
G1 Z10 F200                  ; move Z up 5mm
G90                          ; set movement to absolute mode
M558 P2                      ; set probe to modulated mode 
G30 P0 X27 Y-3   Z-99999     ; define 4 points in a clockwise direction around the bed, starting near (0,0)
G30 P1 X27 Y123  Z-99999
G30 P2 X131 Y123 Z-99999
G30 P3 X131 Y-3  Z-99999 S0  ; finally probe last point, and calculate compensation
G4 P1500                     ; pause to let the web interface catch up

