; homex.g
; called to home the X axis
;
; generated by RepRapFirmware Configuration Tool v2.1.8 on Fri Apr 10 2020 13:58:17 GMT+0200 (Central European Summer Time)
G91               ; relative positioning
G1 S2 Z4 F1800    ; lift Z relative to current position
G1 S1 X-156 F1800 ; move quickly to X axis endstop and stop there (first pass)
G1 S2 X5 F1800    ; go back a few mm
G1 S1 X-156 F360  ; move slowly to X axis endstop once more (second pass)
G1 S2 Z-4 F1800   ; lower Z again
G90               ; absolute positioning

