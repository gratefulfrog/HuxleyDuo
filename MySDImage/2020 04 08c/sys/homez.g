G91               ; set movement to relative mode
G1 Z5 F200        ; move Z up 5mm
G90               ; set movement to absolute mode
;M558 P2          ; set probe to modulated mode 
M558 P2 F20       ; set probe to modulated mode feed rate 20mm/min 2020 03 30 19:00 
;G1 X27 Y-3 F2000   ;  MY VALUES as of 2017 10 24
G1 X22 Y-3 F2000   ;  MY VALUES as of 2020 04 08 19:25
G30               ; home Z, using values from G31 in config.g

